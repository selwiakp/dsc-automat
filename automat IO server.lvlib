﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2020_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2020\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2020_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2020\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="automat1" Type="IO Server">
		<Property Name="atrb" Type="Str">X!-!!!Q1!!!;!!!!"B!!!!1!!!!R!$!!:Q"S!!)!!!!!"B!!!!-!!!!R!(I!&lt;!!#!!!!!!91!!!%!!!!-A!Q!'=!=A!#!!!!!!91!!!$!!!!-A"[!'Q!!A!!!!!'%!!!"!!!!$5!-!"H!()!!A!!!!!'%!!!!Q!!!$5!?A"M!!)!!!!!"B!!!!=!!!"$!']!&lt;!"B!#!!+Q!R!!)!!!!!"B!!!")!!!"$!']!&lt;!"B!#!!=Q"D!'A!&lt;!"P!'1!?A"P!'Y!91!A!#M!-1!#!!!!!!91!!!2!!!!1Q"P!'Y!:A"J!'=!&gt;1"S!'%!&gt;!"J!']!&lt;A"/!'%!&lt;1"F!!91!!!3!!!!2!"4!%-!)!"B!(5!&gt;!"P!'U!91"U!#!!=Q"F!()!&gt;A"F!()!"B!!!!A!!!",!(5!=!!A!'-!&lt;Q"M!'5!!A!!!!!'%!!!%Q!!!%M!&gt;1"Q!#!!9Q"P!'Q!:1!A!(-!9Q"I!'Q!&lt;Q"E!(I!&lt;Q"O!'%!!A!!!!!'%!!!"Q!!!%M!&gt;1"Q!#!!=Q"P!'M!!A!!!!!'%!!!%A!!!%M!&gt;1"Q!#!!=Q"P!'M!)!"T!'-!;!"M!']!:!"[!']!&lt;A"Z!!)!!!!!"B!!!!A!!!",!(5!=!!A!(=!&lt;Q"E!'5!!A!!!!!'%!!!%Q!!!%M!&gt;1"Q!#!!&gt;Q"P!'1!:1!A!(-!9Q"I!'Q!&lt;Q"E!(I!&lt;Q"O!'%!!A!!!!!'%!!!$Q!!!%Q!91"C!&amp;9!31"&amp;!&amp;=!)!"7!'5!=A"T!'E!&lt;Q"O!!-!!!!!!!!!!!!U1!91!!!.!!!!4A"B!(!!=A"B!(=!)!"B!(=!91"S!'E!:1!#!!!!!!91!!!2!!!!5A"P!(I!=!"P!'-!?A"O!'E!;A!A!(I!91"L!(5!=!"Z!!)!!!!!"B!!!!9!!!"4!']!;Q!A!#M!-1!#!!!!!!91!!!2!!!!5Q"P!'M!)!"T!'-!;!"M!']!:!"[!']!&lt;A"Z!#!!+Q!R!!)!!!!!"B!!!")!!!"6!(I!&gt;1"Q!'5!&lt;!"O!'E!;A!A!(!!=A"P!'1!&gt;1"L!(1!?1!#!!!!!!91!!!(!!!!6Q"P!'1!91!A!#M!-1!#!!!!!!91!!!3!!!!6Q"P!'1!91!A!(-!9Q"I!'Q!&lt;Q"E!(I!&lt;Q"O!'%!)!!L!$%!!A!!!!!'%!!!$1!!!&amp;=!?1"C!'E!:1"S!(I!)!"U!']!&gt;Q"B!()!!A!!!!!'%!!!&amp;!!!!&amp;I!91"L!']!2!&amp;D!(I!)!"V!(I!&gt;1"Q!'5!1A&amp;O!'E!91"O!'E!:1!#!!!!!!91!!!-!!!!7A"H!'Q!&lt;Q"T!#!!91"X!'%!=A"J!'5!!A!!!!!</Property>
		<Property Name="className" Type="Str">Custom VI - Periodic</Property>
	</Item>
</Library>
